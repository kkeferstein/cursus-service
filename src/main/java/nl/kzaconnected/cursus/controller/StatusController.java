package nl.kzaconnected.cursus.controller;

import nl.kzaconnected.cursus.model.Dao.Status;
import nl.kzaconnected.cursus.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/statussen")
@CrossOrigin
public class StatusController {

    @Autowired
    private StatusService statusService;

    @GetMapping
    public List<Status> getAllStatussen() {
        return statusService.findAll();
    }
}


