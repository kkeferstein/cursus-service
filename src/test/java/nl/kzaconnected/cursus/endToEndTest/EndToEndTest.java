package nl.kzaconnected.cursus.endToEndTest;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import nl.kzaconnected.cursus.CursusServiceApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

@RunWith(SpringRunner.class)
//@SpringBootTest(
//        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
//        classes = CursusServiceApplication.class)
@AutoConfigureMockMvc
//@DirtiesContext(
//        classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
//@ActiveProfiles("test")
public class EndToEndTest {

    @Value("${test.omgeving.url:http://localhost:8080/api}")
    private String url;
//    @Value("${test.omgeving.port:8080}")
//    private int port;

    @Before
    public void setup() {
        RestAssured.baseURI = url;
//        RestAssured.port = port;
    }

    @Test
    public void testA() {
        Response response = RestAssured
                .when().get("/cursussen");

        response.then().statusCode(200);
    }

    @Test
    public void testB() {

        Response response = RestAssured
                .given().header("content-type", "application/json").body("{\n" +
                        "  \"attitude\": \"Technical attitude\",\n" +
                        "  \"beschrijving\": \"string\",\n" +
                        "  \"cursusdata\": [\n" +
                        "    {\n" +
                        "      \"datum\": \"01-01-2019 18:00:00\"\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"cursusdocenten\": [\n" +
                        "    {\n" +
                        "      \"email\": \"string\",\n" +
                        "      \"naam\": \"string\"\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"functieniveau\": \"1\",\n" +
                        "  \"maxdeelnemers\": 10,\n" +
                        "  \"naam\": \"string\",\n" +
                        "  \"slagingscriterium\": \"Cursusavonden gevolgd\",\n" +
                        "  \"status\": \"Inschrijving geopend\"\n" +
                        "}")
                .when().post("/cursussen");
        response.then().statusCode(201);
    }
}
