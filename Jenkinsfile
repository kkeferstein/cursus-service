#!groovy
def appName = "cursusservice"
def projectO = "kzaconnected-ontwikkel"
def projectA = "kzaconnected-staging"
def projectP = "kzaconnected-productie"
def majorVersion = "1"
def imageVersion = ""

pipeline {
    agent {
        label "maven"
    }
    options {
        skipDefaultCheckout()
    }

    stages {
        stage ('Check out') {
            steps {
                checkout scm
                stash(name: 'ws', includes: "**", excludes: '**/.git/**')
                stash(name: 'openshift', includes: '**/openshift/**')
            }
        }
        stage ('Build code') {
            steps {
                unstash 'ws'
                sh(script: "mvn clean package -Pfatjar -DskipTests")
                stash(name: 'fatjar', includes: '**/target/fatjar.jar')
            }
        }
        stage ('Run unit tests') {
            steps {
                unstash 'ws'
                sh(script: "mvn clean test")
            }
            post {
                always {
                    junit '**/surefire-reports/**/*.xml'
                }
            }
        }
        stage('Build image') {
            steps {
                unstash 'fatjar'
                buildOpenshiftImage(
                        project: projectO,
                        appName: appName
                )
            }
        }
        stage('Create deployment config'){
            when {
                expression {
                    !deploymentConfigExist(
                            project: projectO,
                            appName: appName
                    )
                }
            }
            steps {
                unstash 'openshift'
                createDeploymentConfig(
                        project: projectO,
                        appName: appName,
                        envVar0: 'MAIL_MAINEMAILADRES=kzaconnected@kza.nl',
                        envVar1: "IMAGE=${projectO}/${appName}:latest"
                )
            }
        }
        stage('Deploy image to ontwikkel') {
            steps {
                unstash 'openshift'
                triggerDeployment(
                        project: projectO,
                        appName: appName
                )
            }
        }
        stage('Run end 2 end tests') {
            steps {
                echo "To do!!"
            }
        }
        stage('Tag images') {
            when {
                branch 'master'
            }
            steps {
                script {
                    imageVersion = setImageVersion(majorVersion)
                }
                pushImageTagFromToOpenShiftProject(
                        fromProject: projectO,
                        toProject: projectO,
                        appName: appName,
                        fromTag: 'latest',
                        toTag: imageVersion
                )
            }
        }
        stage('Promote image to staging') {
            when {
                branch 'master'
            }
            steps {
                pushImageTagFromToOpenShiftProject(
                        fromProject: projectO,
                        toProject: projectA,
                        appName: appName,
                        fromTag: imageVersion,
                        toTag: 'latest'
                )
                pushImageTagFromToOpenShiftProject(
                        fromProject: projectO,
                        toProject: projectA,
                        appName: appName,
                        fromTag: imageVersion,
                        toTag: imageVersion
                )
            }
        }
        stage('Deploy new image to staging'){
            when {
                branch 'master'
            }
            steps {
                updateDeploymentConfig(
                        project: projectA,
                        appName: appName,
                        imageTag: imageVersion
                )
                triggerDeployment(
                        project: projectA,
                        appName: appName
                )
            }
        }
        stage('Run end 2 end test on staging') {
            when {
                branch 'master'
            }
            steps {
                echo 'To do'
            }
        }
        stage('Promote image to production') {
            when {
                branch 'master'
            }
            steps {
                pushImageTagFromToOpenShiftProject(
                        fromProject: projectA,
                        toProject: projectP,
                        appName: appName,
                        fromTag: imageVersion,
                        toTag: 'latest'
                )
                pushImageTagFromToOpenShiftProject(
                        fromProject: projectA,
                        toProject: projectP,
                        appName: appName,
                        fromTag: imageVersion,
                        toTag: imageVersion
                )
            }
        }
        stage('Deploy new image to production'){
            when {
                branch 'master'
            }
            steps {
                updateDeploymentConfig(
                        project: projectP,
                        appName: appName,
                        imageTag: imageVersion
                )
                triggerDeployment(
                        project: projectP,
                        appName: appName
                )
            }
        }
    }
}